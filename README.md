Este repositorio es un fork de [Laradock](https://github.com/laradock/laradock), su documentación completa se encuentra en [laradock.io](http://laradock.io/)

## Requirements:
*  **[Download Docker](http://docker.com/)**
    * [for Windows](https://www.docker.com/docker-windows)
    * [for MacOS](https://www.docker.com/docker-mac)

#### Folder structure:
```
├── repository
│   ├── forge-env-docker
│   ├── public
```

#### Files to ignore in your git:
```gitignore

### Docker
/data/

### Environment
.env
```

---

## Installation:
Clonar repositorio git como sub modulo dentro del proyecto
```
git submodule add https://bitbucket.org/lafabricaimaginaria/forge-env-docker.git
```

---

## Build aplication:

```bash
 docker-compose up -d nginx mysql phpmyadmin
```

---

## Access:

### PhpMyAdmin: URL: http://localhost:88/

- Server: `forgeenvdocker_mysql_1`
- User: `default`
- Pass: `secret`